import React from "react";
import { SafeAreaView } from "react-native";
import {
  Layout,
  Text,
  TopNavigation,
  TopNavigationAction,
  Divider,
  Button,
} from "@ui-kitten/components";
import BackIcon from "../components/BackIcon";
import { openDialog } from "../dialogs/utils";
import TestList from "../components/internal/list/TestList";

export default ({ navigation }) => {
  const navigateBack = () => {
    navigation.goBack();
  };
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack} />
  );
  return (
    <SafeAreaView style={{ flex: 1, paddingTop: 50 }}>
      <TopNavigation
        title="Screen 3"
        alignment="center"
        accessoryLeft={BackAction}
      />
      <Divider />

        <TestList />

    </SafeAreaView>
  );
};
