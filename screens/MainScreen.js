import React from "react";
import {
  SafeAreaView,
  Dimensions,
  ImageBackground,
  View,
  Image,
} from "react-native";
import {
  Layout,
  Text,
  Button,
  TopNavigation,
  TopNavigationAction,
  Divider,
  Card,
  Icon,
  StyleService,
  useStyleSheet,
} from "@ui-kitten/components";

import BackIcon from "../components/BackIcon";
const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: "background-basic-color-2",
  },
  productList: {
    paddingHorizontal: 8,
    paddingVertical: 16,
  },
  productItem: {
    margin: 8,
    maxWidth: Dimensions.get("window").width - 10,
    backgroundColor: "background-basic-color-1",
    flex: 1,
  },
  mainProductItem: {
    margin: 8,
    maxWidth: Dimensions.get("window").width - 10,
    backgroundColor: "background-basic-color-1",
    flex: 2,
  },
  imgBg: {
    width: Dimensions.get("window").width - 60,
    minHeight: 100,
    maxHeight: 150,
    paddingRight: 20,
  },
  itemHeader: {
    flex: 4,
    maxHeight: 400,
    width: "100%",
  },
  itemFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
  iconButton: {
    paddingHorizontal: 0,
  },
});

const CartIcon = () => <Icon name="shopping-cart" />;

export default ({ navigation }) => {
  const navigateBack = () => {
    navigation.goBack();
  };
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack} />
  );
  const styles = useStyleSheet(themedStyles);
  const WorkflowImage = () => (
    <ImageBackground
      style={styles.imgBg}
      source={require("../assets/workflow.jpg")}
    />
  );
  const WorkorderImage = () => (
    <ImageBackground
      style={styles.imgBg}
      source={require("../assets/workorder.jpg")}
    />
  );

  const InventoryImage = () => (
    <ImageBackground
      style={styles.imgBg}
      source={require("../assets/inventory.jpg")}
    />
  );

  return (
    <SafeAreaView style={{ flex: 1, paddingTop: 40 }}>
      <TopNavigation
        title="Main"
        alignment="center"
        accessoryLeft={BackAction}
      />
      <Divider />
      <Layout style={{ flex: 1 }}>
        <Card
          footer={() => (
            <View style={styles.itemFooter}>
              <Text category="s1">Workflow</Text>
              <Text appearance="hint" category="c1">
                Check the existing workflow assignments
              </Text>
            </View>
          )}
          onPress={() => navigation.navigate("PO List Screen")}
          style={styles.productItem}
        >
          <WorkflowImage />
        </Card>
        <Card
          footer={() => (
            <View style={styles.itemFooter}>
              <Text category="s1">Work Orders</Text>
              <Text appearance="hint" category="c1">
                Active and assigned Work Orderes
              </Text>
            </View>
          )}
          onPress={() => navigation.navigate("PO List Screen")}
          style={styles.productItem}
        >
          <WorkorderImage />
        </Card>
        <Card
          footer={() => (
            <View style={styles.itemFooter}>
              <Text category="s1">Inventory</Text>
              <Text appearance="hint" category="c1">
                Check and adjust inventory status
              </Text>
            </View>
          )}
          onPress={() => navigation.navigate("Screen 3")}
          style={styles.productItem}
        >
          <InventoryImage />
        </Card>
        <Card onPress={() => navigation.navigate("Qbe Section")}>
          <Text>Search</Text>
        </Card>
      </Layout>
    </SafeAreaView>
  );
};
