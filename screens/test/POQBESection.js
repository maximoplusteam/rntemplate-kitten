import React from "react";
import { QbeSection } from "../../components/Section";
import { SafeAreaView } from "react-native";
import {
  TopNavigation,
  TopNavigationAction,
  Divider,
} from "@ui-kitten/components";

import BackIcon from "../../components/BackIcon";

export default ({ navigation }) => {
  const navigateBack = () => {
    navigation.goBack();
  };
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack} />
  );
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TopNavigation
        title="PO Search"
        alignment="center"
        accessoryLeft={BackAction}
      />
      <Divider />
      <QbeSection
        container="pocont"
        columns={["ponum", "description", "status", "shipvia"]}
        metadata={{
          SHIPVIA: {
            hasLookup: true,
            listTemplate: "qbevaluelist",
            offlineReturnColumn: "VALUE",
          },
          STATUS: {
            hasLookup: true,
            listTemplate: "qbevaluelist",
            offlineReturnColumn: "VALUE",
          },
        }}
      />
    </SafeAreaView>
  );
};
