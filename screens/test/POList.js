import React from "react";
import MaxList from "../../components/MaxList";
import { SafeAreaView } from "react-native";
import {
  Layout,
  Text,
  Button,
  TopNavigation,
  TopNavigationAction,
  Divider,
} from "@ui-kitten/components";
import BackIcon from "../../components/BackIcon";

export default ({ navigation }) => {
  const navigateBack = () => {
    navigation.goBack();
  };
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack} />
  );
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TopNavigation
        title="List of POs"
        alignment="center"
        accessoryLeft={BackAction}
      />
      <Divider />

      <MaxList
        listTemplate="po"
        container="pocont"
        columns={["ponum", "description", "status"]}
        norows={20}
        initdata={true}
      />
    </SafeAreaView>
  );
};
