import React from "react";
import { Section } from "../../components/Section";
import { SafeAreaView } from "react-native";
import {
  TopNavigation,
  TopNavigationAction,
  Divider,
  Icon,
} from "@ui-kitten/components";
import {
  openPhotoUpload,
  openDocumentsUpload,
  openDocumentViewer,
  openWorkflow,
} from "../../dialogs/utils";
import BackIcon from "../../components/BackIcon";
import Workflow from "../../components/Worfklow";

const PhotoIcon = (props) => <Icon name="camera" {...props} />;
const DocumentAddIcon = (props) => <Icon name="attach-outline" {...props} />;
const ViewDocumentsIcon = (props) => <Icon name="archive-outline" {...props} />;
const WorkflowIcon = (props) => <Icon name="shuffle-2-outline" {...props} />;
const TakePhotoAction = () => (
  <TopNavigationAction
    icon={PhotoIcon}
    onPress={() => openPhotoUpload("pocont")}
  />
);
const UploadDocumentAction = () => (
  <TopNavigationAction
    icon={DocumentAddIcon}
    onPress={() => openDocumentsUpload("pocont", "Attachments")}
  />
);
const ViewDocumentsAction = () => (
  <TopNavigationAction
    icon={ViewDocumentsIcon}
    onPress={() => openDocumentViewer("pocont")}
  />
);

const WorkflowAction = () => (
  <TopNavigationAction
    icon={WorkflowIcon}
    onPress={() => openWorkflow("posingle", "POSTATUS")}
  />
);

export default ({ navigation }) => {
  const navigateBack = () => {
    navigation.goBack();
  };
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack} />
  );
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TopNavigation
        title="PO Details"
        alignment="center"
        accessoryLeft={BackAction}
        accessoryRight={WorkflowAction}
      />
      <Divider />
      <Section
        container="posingle"
        columns={[
          "ponum",
          "description",
          "status",
          "shipvia",
          "orderdate",
          "vendor",
          "vendor.phone",
          "paymentterms",
        ]}
        metadata={{
          "VENDOR.PHONE": { phonenum: true },
          SHIPVIA: {
            hasLookup: true,
            listTemplate: "valuelist",
            offlineReturnColumn: "VALUE",
          },
          PAYMENTTERMS: { barcode: true },
        }}
      />
    </SafeAreaView>
  );
};
