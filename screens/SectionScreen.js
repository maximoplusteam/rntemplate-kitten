import React from "react";
import { SafeAreaView } from "react-native";
import {
  Layout,
  Text,
  Button,
  TopNavigation,
  Divider,
} from "@ui-kitten/components";
import TestSection from "../components/internal/section/TestSection";

export default ({ navigation }) => (
  <SafeAreaView style={{ flex: 1 }}>
    <TopNavigation title="First Screen" alignment="center" />
    <Divider />
    <Layout style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <TestSection />
      <Button
        onPress={() => {
          console.log("scr2");
          navigation.navigate("Screen 3");
        }}
      >
        SCR2
      </Button>
    </Layout>
  </SafeAreaView>
);
