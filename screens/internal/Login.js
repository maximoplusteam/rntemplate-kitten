import React, { useState } from "react";
import {
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Image,
} from "react-native";
import {
  Button,
  Input,
  Layout,
  StyleService,
  Text,
  useStyleSheet,
  Icon,
} from "@ui-kitten/components";
import { showMessage } from "react-native-flash-message";
import { maxLogin } from "mplus-react";

const Login = ({ setLoggedIn }) => {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [passwordVisible, setPasswordVisible] = useState(false);
  const PersonIcon = <Icon name="person" />;
  const onPasswordIconPress = () => {
    setPasswordVisible(!passwordVisible);
  };
  const styles = useStyleSheet(themedStyles);

  const renderPasswordIcon = (props) => (
    <TouchableWithoutFeedback onPress={onPasswordIconPress}>
      <Icon {...props} name={passwordVisible ? "eye-off" : "eye"} />
    </TouchableWithoutFeedback>
  );

  const login = () => {
    maxLogin(
      userName,
      password,
      () => {
        setLoggedIn(true);
      },
      (err) =>
        showMessage({
          message: "Invalid Username or Password",
          type: "error",
          position: "top",
          duration: 2000,
          color: "white",
          backgroundColor: "red",
          icon: "warning",
        })
    );
  };

  return (
    <KeyboardAvoidingView style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          source={require("../../assets/white_logo_transparent_background_small.png")}
          style={styles.logo}
        />
        <Text category="h1" status="control"></Text>
        <Text style={styles.signInLabel} category="s1" status="control">
          Sign in to your account
        </Text>
      </View>
      <Layout style={styles.formContainer} level="1">
        <Input
          placeholder="Username"
          accessoryRight={PersonIcon}
          value={userName}
          onChangeText={setUserName}
        />
        <Input
          style={styles.passwordInput}
          placeholder="Password"
          accessoryRight={renderPasswordIcon}
          value={password}
          secureTextEntry={!passwordVisible}
          onChangeText={setPassword}
        />
        <View style={{marginTop:20}}>
          <Button style={styles.signInButton} size="giant" onPress={login}>
            SIGN IN
          </Button>
        </View>
      </Layout>
    </KeyboardAvoidingView>
  );
};

const themedStyles = StyleService.create({
  container: {
    backgroundColor: "background-basic-color-1",
    height: "100%",
  },
  headerContainer: {
    justifyContent: "center",
    alignItems: "center",
    minHeight: 216,
    backgroundColor: "color-primary-default",
  },
  formContainer: {
    flex: 1,
    paddingTop: 32,
    paddingHorizontal: 16,
  },
  signInLabel: {
    marginTop: 16,
  },
  signInButton: {
    marginHorizontal: 16,
  },
  signUpButton: {
    marginVertical: 12,
    marginHorizontal: 16,
  },
  forgotPasswordContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  passwordInput: {
    marginTop: 16,
  },
  forgotPasswordButton: {
    paddingHorizontal: 0,
  },
  endView: {
    flex: 2,
    flexDirection: "column",
    marginBottom: "40%",
    justifyContent: "flex-end",
  },
  logo: {
    width: "60%",
    height: 40,
    resizeMode: "contain",
  },
});

Login.displayName = "Login";

export default Login;
