import React from "react";
import { AppContainer, SingleMboContainer, RelContainer } from "mplus-react";

export default () => {
  return (
    <>
      <AppContainer
        id="pocont"
        mboname="po"
        appname="po"
        offlineenabled={false}
      />
      <SingleMboContainer id="posingle" container="pocont"/>
    </>
  );
};
