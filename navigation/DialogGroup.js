import TestDialog from "../dialogs/TestDialog";
import ListDialog from "../dialogs/internal/ListDialog";

export const getDialogs = () => [
  { name: "Dialog1", component: TestDialog },
  { name: "list", component: ListDialog },
];
