import React, { useState } from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import MainScreen from "../screens/MainScreen";
import SectionScreen from "../screens/SectionScreen";
import Screen3 from "../screens/Screen3";
import { getDialogs } from "./DialogGroup";
import Login from "../screens/internal/Login";
import { setOnLoggedOff } from "mplus-react";
import POList from "../screens/test/POList";
import POSection from "../screens/test/POSection";
import POQBESection from "../screens/test/POQBESection";
import ListDialog from "../dialogs/internal/ListDialog";
import QbeListDialog from "../dialogs/internal/QbeListDialog";
import BarCodeScan from "../dialogs/internal/BarCodeScan";
import PhotoUpload from "../dialogs/internal/PhotoUpload";
import DocumentUploader from "../dialogs/internal/DocumentUploader";
import DocumentViewer from "../dialogs/internal/DocumentViewer";
import WorkflowDialog from "../dialogs/internal/WorkflowDialog";

const Stack = createNativeStackNavigator();

export const { Navigator, Screen, Group } = Stack;

export default () => {
  const [loggedIn, setLoggedIn] = useState(true);
  setOnLoggedOff(() => {
    setLoggedIn(false);
  });
  const LoginComp = () => <Login setLoggedIn={setLoggedIn} />;
  return (
    <Navigator screenOptions={{ headerShown: false }}>
      {loggedIn ? (
        <Group>
          <Screen name="Main" component={MainScreen} />
          <Screen name="PO List Screen" component={POList} />
          <Screen name="Screen 3" component={POSection} />
          <Screen name="Qbe Section" component={POQBESection} />
        </Group>
      ) : (
        <Group>
          <Screen name="login" component={LoginComp} />
        </Group>
      )}
      <Group>
        <Screen name="list" component={ListDialog} />
        <Screen name="qbelist" component={QbeListDialog} />
        <Screen name="documentsUpload" component={DocumentUploader} />
        <Screen name="documentsViewer" component={DocumentViewer} />
        <Screen name="workflow" component={WorkflowDialog} />
      </Group>
      <Group screenOptions={{ presentation: "modal" }}>
        <Screen name="barcodeScan" component={BarCodeScan} />
        <Screen name="photoUpload" component={PhotoUpload} />
      </Group>
    </Navigator>
  );
};
