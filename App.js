import React from "react";
import * as eva from "@eva-design/eva";
import { ApplicationProvider, IconRegistry } from "@ui-kitten/components";
import { NavigationContainer } from "@react-navigation/native";
import FirstStack from "./navigation/FirstStack";
import { EvaIconsPack } from "@ui-kitten/eva-icons";
import { setExternalRootContext } from "mplus-react";
import { ContextPool } from "react-multiple-contexts";
import DialogWrapper from "./dialogs/internal/DialogWrapper";
import { dialogWrapperRef } from "./dialogs/utils";
import "./utils/init";
import FlashMessage from "react-native-flash-message";
import Containers from "./Containers";

const rootContext = React.createContext(null);
setExternalRootContext(rootContext);

export default () => (
  <ContextPool rootContext={rootContext} initialSize={10} minimumFree={3}>
    <NavigationContainer>
      <IconRegistry icons={EvaIconsPack} />
      <Containers />
      <DialogWrapper ref={dialogWrapperRef} />
      <ApplicationProvider {...eva} theme={eva.light}>
        <FirstStack />
      </ApplicationProvider>
    </NavigationContainer>
    <FlashMessage position="bottom" />
  </ContextPool>
);
