import React from "react";
import { SafeAreaView } from "react-native";
import {
  Layout,
  Text,
  TopNavigation,
  TopNavigationAction,
  Divider,
  Button,
} from "@ui-kitten/components";
import BackIcon from "../components/BackIcon";
import { getDialogProps, openDialog, closeDialog } from "./utils";

export default ({ navigation, route }) => {
  const params = getDialogProps(route);
  const navigateBack = () => {
    navigation.goBack();
  };
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={closeDialog} />
  );
  return (
    <SafeAreaView style={{ flex: 1, paddingTop: 50 }}>
      <TopNavigation
        title="Dialog 1"
        alignment="center"
        accessoryLeft={BackAction}
      />
      <Divider />
      <Layout
        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
      >
        <Text category="h1">{params && params.text}</Text>
        <Button
          onPress={(_) => openDialog("Dialog1", { text: "Something else" })}
        >
          Test this
        </Button>
      </Layout>
    </SafeAreaView>
  );
};
