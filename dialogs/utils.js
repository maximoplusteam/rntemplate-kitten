import { createRef } from "react";
export { getDialogProps } from "./internal/DialogWrapper";

export const dialogWrapperRef = createRef();

export const openDialog = (dialogName, dialogData) => {
  dialogWrapperRef.current.openDialog({ type: dialogName, ...dialogData });
};

export const closeDialog = () => {
  dialogWrapperRef.current.closeDialog();
};

export const openBarcodeScan = (onScan) => {
  openDialog("barcodeScan", { onScan });
};

export const openPhotoUpload = (container) => {
  openDialog("photoUpload", { container });
};

export const openDocumentsUpload = (container, folder) => {
  openDialog("documentsUpload", { container, folder });
};

export const openDocumentViewer = (container) => {
  openDialog("documentsViewer", { container });
};

export const openOfflineErrors = (errors) => {
  openDialog("offlineError", { errors });
  //TODO copy Offline errors dialog from previous template
};

export const openWorkflow = (container, processName) => {
  openDialog("workflow", { container, processName });
};
