import React from "react";
import { SafeAreaView } from "react-native";
import {
  TopNavigation,
  TopNavigationAction,
  Divider,
} from "@ui-kitten/components";
import BackIcon from "../../components/BackIcon";
import { getDialogProps, closeDialog } from "../utils";
import MaxList from "../../components/MaxList";
import {Icon} from "@ui-kitten/components";

const OKIcon=(props)=><Icon {...props} name="funnel-outline"/>;

export default ({ route }) => {
  const d = getDialogProps(route);

  const BackAction = () => (
    <TopNavigationAction icon={OKIcon} onPress={closeDialog} />
  );
  return (
    <SafeAreaView style={{ flex: 1, paddingTop: 50 }}>
      <TopNavigation
        title="Pick one or more values"
        alignment="center"
        accessoryLeft={BackAction}
      />
      <Divider />
      <MaxList
        norows={20}
        listTemplate={d.metadata.listTemplate}
        filterTemplate={d.metadata.filterTemplate}
        maxcontainer={d.listContainer}
        initdata={true}
        columns={d.dialogCols}
        selectableF={d.defaultAction}
        showWaiting={true}
      />
    </SafeAreaView>
  );
};
