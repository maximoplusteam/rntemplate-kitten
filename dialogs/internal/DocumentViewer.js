import React, { useState } from "react";
import MaxList from "../../components/MaxList";
import {
  View,
  Image,
  Text,
  Linking,
  Platform,
  SafeAreaView,
} from "react-native";
import { RelContainer, getDownloadURL, getLocalValue } from "mplus-react";
import { closeDialog, getDialogProps } from "../utils";
import { getServerRoot } from "../../utils/boot";
import BackIcon from "../../components/BackIcon";
import {
  TopNavigation,
  TopNavigationAction,
  Divider,
} from "@ui-kitten/components";

export default ({ route }) => {
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={closeDialog} />
  );
  const openDocument = async () => {
    const maximoURL = await getLocalValue("doclinks", "urlname"); //TO find the file type
    const fileExt = maximoURL.substr(maximoURL.lastIndexOf(".") + 1);
    const _isImage =
      ["jpg", "JPG", "jpeg", "JPEF", "png", "PNG", "gif", "GIF"].indexOf(
        fileExt
      ) != -1;
    const isPdf = ["pdf", "PDF"].indexOf(fileExt) != -1;
    const dlURL = await getDownloadURL("doclinks", "doclinksredirect");
    const newDLURL = await fetch(dlURL, { credentials: "include" });
    const dlTxt =
      getServerRoot() + "/" + (await newDLURL.text()) + "?ver=" + Date.now();
    Linking.openURL(dlTxt);
  };

  const { container } = getDialogProps(route);
  if (!container) {
    return (
      <View>
        <Text>No container specified for the dialog</Text>
      </View>
    );
  }
  return (
    <SafeAreaView style={{ flex: 1, paddingTop: 50 }}>
      <TopNavigation
        title="View documents"
        alignment="center"
        accessoryLeft={BackAction}
      />
      <Divider />
      <MaxList
        listTemplate="doclinks"
        container="doclinks"
        norows={20}
        initdata={true}
        columns={[
          "document",
          "doctype",
          "description",
          "changeby",
          "changedate",
          "urlname",
        ]}
        selectableF={openDocument}
      />
      <RelContainer
        id="doclinks"
        container={container}
        relationship="doclinks"
      />
    </SafeAreaView>
  );
};
