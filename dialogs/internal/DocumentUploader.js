import React, { useState } from "react";
import { getDocumentAsync } from "expo-document-picker";
import {
  Button,
  List,
  Icon,
  ListItem,
  TopNavigation,
  TopNavigationAction,
  Divider,
} from "@ui-kitten/components";
import { Platform, SafeAreaView } from "react-native";
import { uploadFile, save } from "mplus-react";
import { getDialogProps, closeDialog } from "../utils";
import BackIcon from "../../components/BackIcon";

export default ({ route }) => {
  const [files, setFiles] = useState([]);
  const [uploading, setUploading] = useState(false);

  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={closeDialog} />
  );

  const uploadDocuments = async () => {
    const { container, folder } = getDialogProps(route);
    setUploading(true);
    for (let j = 0; j < files.length; j++) {
      const fileItem = files[j];
      const file =
        Platform.OS == "web"
          ? fileItem.file
          : {
              name: fileItem.name,
              uri: fileItem.uri,
              type: "application/octet-stream",
            };
      try {
        fileItem.status = "uploading";
        const uploaded = await uploadFile(container, "doclinks", file, folder);
        fileItem.status = "uploaded";
      } catch (e) {
        console.log(e);
        fileItem.status = "error";
      }
      const newFiles = [...files];
      newFiles[j] = fileItem;
      setFiles(newFiles);

      save(container);
    }
    setUploading(false);
  };

  const renderFileItem = ({ item, index }) => {
    const delAction = () => {
      let copiedState = [...files];
      copiedState.splice(index, 1);
      setFiles(copiedState);
    };
    let iconName = uploading ? "loader-outline" : "trash-2-outline";
    let fileAction = uploading ? null : delAction;

    let buttonStatus = null;
    if (item.status == "uploading") {
      iconName = "loader-outline";
      fileAction = null;
    }
    if (item.status == "error") {
      iconName = "alert-triangle-outline";
      fileAction = null;
      buttonStatus = "danger";
    }
    if (item.status == "uploaded") {
      iconName = "checkmark-circle-outline";
      fileAction = null;
      buttonStatus = "success";
    }

    const leftIcon = (props) => (
      <Button
        onPress={fileAction}
        appearance="ghost"
        accessoryLeft={(props) => <Icon name={iconName} {...props} />}
        status={buttonStatus}
      ></Button>
    );
    return <ListItem title={item.name} accessoryLeft={leftIcon}></ListItem>;
  };

  const readDocument = async () => {
    const picked = await getDocumentAsync({ copyToCacheDirectory: false });
    if (picked.type != "cancel") {
      setFiles([...files, picked]);
    }
  };
  return (
    <SafeAreaView style={{ flex: 1, paddingTop: 50 }}>
      <TopNavigation
        title="Choose files"
        alignment="center"
        accessoryLeft={BackAction}
      />
      <Divider />
      <List
        ItemSeparatorComponent={Divider}
        renderItem={renderFileItem}
        data={files}
      />
      {uploading ? null : (
        <Button onPress={readDocument} appearance="ghost">
          Choose a document
        </Button>
      )}
      {!uploading && files.length != 0 ? (
        <Button
          appearance="ghost"
          onPress={uploadDocuments}
          accessoryLeft={(props) => (
            <Icon name="cloud-upload-outline" {...props} />
          )}
        >
          Upload documents
        </Button>
      ) : null}
    </SafeAreaView>
  );
};
