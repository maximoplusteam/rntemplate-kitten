import React, { useEffect, useState, useRef } from "react";
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
} from "react-native";
import { Camera } from "expo-camera";
import { closeDialog, getDialogProps } from "../utils";
import { Text, Icon } from "@ui-kitten/components";

import { uploadFile, save } from "mplus-react";
import { showMessage, hideMessage } from "react-native-flash-message";
import { decode } from "base64-arraybuffer";

const styles = StyleSheet.create({
  icon: {
    width: 84,
    height: 84,
  },
  smallIcon: { width: 32, height: 32 },
  uploading: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: "black",
    justifyContent: "center",
    alignItems: "center",
    color: "white",
  },
});

export default ({ route, navigation }) => {
  const CameraRef = useRef();
  const takePicture = () => {
    if (CameraRef.current) {
      CameraRef.current.takePictureAsync({
        onPictureSaved,
      });
      setDisplayShootButton(false);
      setUploadReady(true);
    }
  };
  const [hasCameraPermission, setCameraPermission] = useState(null);
  const photoUri = useRef(null);
  const [uploading, setUploading] = useState(false);
  const [displayShootButton, setDisplayShootButton] = useState(true);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const [uploadReady, setUploadReady] = useState(false);
  const { container } = getDialogProps(route);
  const [photoTaken, setPhotoTaken] = useState(false);
  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setCameraPermission(status === "granted");
    })();
  }, []);
  const onPictureSaved = async (photo) => {
    photoUri.current = photo.uri;
    setDisplayShootButton(true);
    setPhotoTaken(true);
  };
  const takeAgain = () => {
    console.log("Taking again");
    setUploading(false);
    photoUri.current = null;
    setDisplayShootButton(true);
    setPhotoTaken(false);
  };
  const upload = () => {
    if (!photoUri.current) {
      alert("No photo yet taken");
      return;
    }

    let file = null;

    if (Platform.OS === "web") {
      const fileType = photoUri.current.match(/data:(.*);/)[1];
      const fileExtension = fileType.substr(fileType.lastIndexOf("/") + 1);
      const rawImageData = photoUri.current.replace(
        /^data:image\/\w+;base64,/,
        ""
      );
      const arrayBuf = decode(rawImageData);
      const fileName = "IMG-" + new Date().valueOf() + "." + fileExtension;
      file = new File([arrayBuf], fileName, { type: fileType });
    } else {
      const fileName = photoUri.current.substr(
        photoUri.current.lastIndexOf("/") + 1
      );
      const fileType =
        "image/" + fileName.substr(fileName.lastIndexOf(".") + 1);
      file = {
        name: fileName,
        uri: photoUri.current,
        type: fileType,
      };
    }
    setUploading(true);

    uploadFile(container, "doclinks", file, "Photos")
      .then(() => {
        save(container);
        setUploading(false);
        showMessage("Photo uploaded");
        closeDialog();
      })
      .catch((err) => {
        setUploading(false);
        alert(err);
      });
  };
  const CameraIcon = (props) => (
    <Icon
      fill="#FFFFFF"
      name="camera-outline"
      style={styles.smallIcon}
      {...props}
    />
  );
  const UploadIcon = (props) => (
    <Icon
      fill="#FFFFFF"
      name="cloud-upload-outline"
      style={styles.smallIcon}
      {...props}
    />
  );
  const BackIcon2 = (props) => (
    <Icon
      fill="#FFFFFF"
      {...props}
      name="arrow-ios-back"
      style={styles.smallIcon}
    />
  );
  const flip = () => {
    setType(
      type === Camera.Constants.Type.back
        ? Camera.Constants.Type.front
        : Camera.Constants.Type.back
    );
  };

  if (hasCameraPermission === null) {
    return <View />;
  }
  if (hasCameraPermission === false) {
    return <Text>No access to camera</Text>;
  }
  if (photoTaken) {
    return (
      <View style={{ flex: 1, bac: "black" }}>
        <Image style={{ flex: 10 }} source={{ uri: photoUri.current }} />
        <View
          style={{
            flex: 1,
            alignSelf: "flex-end",
            alignItems: "stretch",
            flexDirection: "row",
            paddingBottom: 10,
            backgroundColor: "black",
          }}
        >
          <TouchableOpacity
            style={{ flex: 1, alignSelf: "center", alignItems: "flex-start" }}
            onPress={closeDialog}
          >
            <BackIcon2 />
          </TouchableOpacity>
          <TouchableOpacity
            style={{ flex: 1, alignSelf: "center", alignItems: "center" }}
            onPress={upload}
          >
            <UploadIcon />
          </TouchableOpacity>
          <TouchableOpacity
            style={{ flex: 1, alignSelf: "center", alignItems: "flex-end" }}
            onPress={takeAgain}
          >
            <CameraIcon />
          </TouchableOpacity>
        </View>

        {uploading && (
          <View style={styles.uploading}>
            <ActivityIndicator size="large" />
          </View>
        )}
      </View>
    );
  }
  const ShootIcon = (props) => (
    <Icon
      style={styles.icon}
      fill="#FFFFFF"
      name="radio-button-on"
      {...props}
    />
  );
  const FlipIcon = (props) => (
    <Icon
      name="flip-2-outline"
      fill="#FFFFFF"
      {...props}
      style={styles.smallIcon}
    />
  );

  const shootButton = displayShootButton ? (
    <View
      style={{
        flex: 1,
        alignSelf: "flex-end",
        alignItems: "stretch",
        flexDirection: "row",
        paddingBottom: 10,
        margin: 5,
      }}
    >
      <TouchableOpacity
        style={{ flex: 1, alignSelf: "center", alignItems: "flex-start" }}
        onPress={closeDialog}
      >
        <BackIcon2 />
      </TouchableOpacity>
      <TouchableOpacity
        style={{ flex: 1, alignSelf: "center", alignItems: "center" }}
        onPress={takePicture}
      >
        <ShootIcon />
      </TouchableOpacity>
      <TouchableOpacity
        style={{ flex: 1, alignSelf: "center", alignItems: "flex-end" }}
        onPress={flip}
      >
        <FlipIcon />
      </TouchableOpacity>
    </View>
  ) : null;

  return (
    <View style={{ flex: 1 }}>
      <Camera style={{ flex: 1 }} type={type} ref={CameraRef}>
        <View
          style={{
            flex: 1,
            backgroundColor: "transparent",
            color: "white",
            flexDirection: "row",
          }}
        >
          {shootButton}
        </View>
      </Camera>
    </View>
  );
};
