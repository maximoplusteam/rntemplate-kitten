import React from "react";
import { SafeAreaView } from "react-native";
import {
  TopNavigation,
  TopNavigationAction,
  Divider,
} from "@ui-kitten/components";
import BackIcon from "../../components/BackIcon";
import { getDialogProps, closeDialog } from "../utils";
import MaxList from "../../components/MaxList";

//TODO filter for list dialog
export default ({ route }) => {
  const d = getDialogProps(route);

  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={closeDialog} />
  );
  return (
    <SafeAreaView style={{ flex: 1, paddingTop: 50 }}>
      <TopNavigation
        title="Pick a value"
        alignment="center"
        accessoryLeft={BackAction}
      />
      <Divider />
      <MaxList
        norows={20}
        listTemplate={d.field.getMetadata().listTemplate}
        filterTemplate={d.field.getMetadata().filterTemplate}
        maxcontainer={d.listContainer}
        initdata={true}
        columns={d.dialogCols}
        selectableF={d.defaultAction}
        showWaiting={true}
      />
    </SafeAreaView>
  );
};
