import React, { PureComponent } from "react";
import { getDialogHolder } from "mplus-react";
import { useNavigation } from "@react-navigation/native";

class DialogWrapper extends PureComponent {
  render() {
    return null;
  }

  componentDidUpdate(prevProps) {
    const { navigation } = this.props;
    if (this.props.dialogs) {
      if (
        prevProps.dialogs &&
        this.props.dialogs.length < prevProps.dialogs.length
      ) {
        const dialogName =
          prevProps.dialogs[prevProps.dialogs.length - 1]["type"];
        _closeDialog(navigation, dialogName);
      }
      if (
        !prevProps.dialogs ||
        this.props.dialogs.length > prevProps.dialogs.length
      ) {
        const currDialog =
          this.props.dialogs &&
          this.props.dialogs[this.props.dialogs.length - 1];
        if (currDialog) {
          _openDialog(navigation, currDialog.type, { ...currDialog });
        }
      }
    }
  }
}

const WrappedWithNavigation = (props) => {
  const navigation = useNavigation();
  return <DialogWrapper {...props} navigation={navigation} />;
};

function uuidv4() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

export default getDialogHolder(WrappedWithNavigation, null, true);

const dialogCounter = {};
//Opening the same dialog for the same time means we will
// be opening the same dialog with the different params
const navigationProps = {};
const navigationParamIdStacks = {};

const internalOpenDialog = (navigation, dialogName, dialogData) => {
  const paramId = uuidv4();
  navigationProps[paramId] = dialogData;
  let nps = navigationParamIdStacks[dialogName];
  if (!nps) nps = [];
  nps.push(paramId);
  navigationParamIdStacks[dialogName] = nps;
  navigation.navigate(dialogName, { paramId });
};

const internalCloseDialog = (dialogName) => {
  let nps = navigationParamIdStacks[dialogName];
  let paramId = nps.pop();
  delete navigationProps[paramId];
};

const _openDialog = (navigation, dialogName, dialogData) => {
  let dialogParamStack = dialogCounter[dialogName];
  if (!dialogParamStack) {
    dialogParamStack = [];
  }
  dialogParamStack.push(dialogData);
  dialogCounter[dialogName] = dialogParamStack;
  internalOpenDialog(navigation, dialogName, dialogData);
};

const _closeDialog = (navigation, dialogName) => {
  internalCloseDialog(dialogName);
  let dialogParamStack = dialogCounter[dialogName];
  if (!dialogParamStack) {
    navigation.goBack();
    return;
  }
  dialogParamStack.pop();
  dialogCounter[dialogName] = dialogParamStack;
  if (dialogParamStack.length === 0) {
    navigation.goBack();
    return;
  }
  const lastParam = dialogParamStack[dialogParamStack.length - 1];
  internalOpenDialog(navigation, dialogName, lastParam);
};

export const getDialogProps = (route) => {
  const paramId = route.params && route.params["paramId"];
  return paramId && navigationProps[paramId];
};
