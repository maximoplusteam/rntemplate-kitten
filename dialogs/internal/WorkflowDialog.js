import React from "react";
import { SafeAreaView } from "react-native";
import {
  TopNavigation,
  TopNavigationAction,
  Divider,
} from "@ui-kitten/components";
import BackIcon from "../../components/BackIcon";
import { getDialogProps, closeDialog } from "../utils";
import Workflow from "../../components/Worfklow";

export default ({ route }) => {
  const { container, processName } = getDialogProps(route);
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={closeDialog} />
  );
  return (
    <SafeAreaView style={{ flex: 1, paddingTop: 50 }}>
      <TopNavigation
        title="Workflow"
        alignment="center"
        accessoryLeft={BackAction}
      />
      <Divider />
      <Workflow container={container} processName={processName} />
    </SafeAreaView>
  );
};
