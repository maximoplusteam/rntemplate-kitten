import React, { useEffect, useState } from "react";
import { SafeAreaView } from "react-native";
import {
  TopNavigation,
  TopNavigationAction,
  Divider,
  Text,
} from "@ui-kitten/components";
import BackIcon from "../../components/BackIcon";
import { getDialogProps, closeDialog } from "../utils";
import { BarCodeScanner } from "expo-barcode-scanner";
import { View, StyleSheet } from "react-native";

export default ({ route }) => {
  const handleBarCodeScanned = ({ type, data }) => {
    console.log(data);
    const { onScan } = getDialogProps(route);
    if (onScan && typeof onScan === "function") {
      console.log("entered onScan");
      onScan(data);
    }
    closeDialog();
  };
  const [hasPermission, setHasPermission] = useState(null);
  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);
  const d = getDialogProps(route);
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={closeDialog} />
  );
  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }
  return (
    <BarCodeScanner
      onBarCodeScanned={handleBarCodeScanned}
      style={StyleSheet.absoluteFillObject}
    />
  );
};
