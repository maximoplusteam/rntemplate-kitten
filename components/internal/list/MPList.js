import React, { useState } from "react";
import {
  View,
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import { List, Divider } from "@ui-kitten/components";
import ListTemplates from "../../ListTemplates";

const MPListItem = ({
  listTemplate,
  navigation,
  navigate,
  rowAction,
  mxrow,
  data,
}) => {
  const ListTemplate = ListTemplates[listTemplate];
  const _onPress = () => {
    console.log("Touchable opacity onpress");
    rowAction(mxrow);
    if (navigate) {
      navigate(navigation); //???maybe just the screen id
    }
  };
  return <ListTemplate {...data} onPress={_onPress} />;
};

export default ({
  options,
  label,
  waiting,
  data,
  listTemplate,
  navigate,
  fetchMore,
}) => {
  const [fetching, setFetching] = useState(null);
  const FooterWait = () => {
    return waiting ? (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          paddingVertical: 10,
        }}
      >
        <ActivityIndicator animating size="small" />
      </View>
    ) : null;
  };

  return (
    <List
      data={data}
      ListFooterComponent={FooterWait}
      ItemSeparatorComponent={Divider}
      renderItem={({ item }) => (
        <MPListItem {...item} listTemplate={listTemplate} navigate={navigate} />
      )}
      onEndReachedThreshold={1}
      onEndReached={({ distanceFromEnd }) => {
        if (distanceFromEnd <= 0) return;
        requestAnimationFrame((_) => {
          if (fetching === null) {
            //initially don't fetch more, this is triggered even first time on render
            setFetching(false);
            return;
          } else {
            if (!fetching) {
              setFetching(true);
              fetchMore(5);
              setTimeout(() => {
                setFetching(false);
              }, 200);
            }
          }
        });
      }}
    />
  );
};
