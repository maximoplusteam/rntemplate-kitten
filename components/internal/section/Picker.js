import React, { useState, useEffect } from "react";
import { View, StyleSheet } from "react-native";
import { Text, Select, SelectItem, IndexPath } from "@ui-kitten/components";

import { getPickerList } from "mplus-react";

/* HOC can't use hooks, we need to make ordinary function */
const SPicker = (props) => {
  const [picked, setPicked] = useState(props.pickerValue);
  const [selectedIndex, setSelectedIndex] = useState(new IndexPath(0));

  const onSelectedIndex = (_selectedIndex) => {
    const rowNum = _selectedIndex.row;

    setSelectedIndex(_selectedIndex);
    if (!props.rows || props.rows.length === 0) {
      return;
    }
    const rows = props.rows;
    const pickerValue = props.pickerValue;
    useEffect(() => {
      if (!rows || !pickerValue || rows.length == 0) {
        return;
      }
      let ind = -1;
      for (let j = 0; j < rows.length; j++) {
        const { value } = rows[j];
        if (value === pickerValue) {
          ind = j;
        }
      }
      if (ind != -1) {
        setSelectedIndex(ind);
      }
    }, [rows, pickerValue]);
    const chosenValue = props.rows[rowNum]["value"];
    props.changeListener(chosenValue);
  };

  return (
    <View>
      <Text>{props.label}</Text>
      <Select
        onSelect={onSelectedIndex}
        selectedIndex={selectedIndex}
        selectedValue={props.pickerValue}
        onValueChange={(itemValue, itemIndex) => {
          props.changeListener(itemValue);
        }}
      >
        {props.rows.map(({ label, value }) => (
          <SelectItem key={value} title={label} />
        ))}
      </Select>
    </View>
  );
};

const PickerList = getPickerList(
  (label, selected, optionKey, optionVal, changeListener) => {
    return {
      label,
      selected,
      value: optionKey,
      label: optionVal,
      changeListener,
    };
  },
  (label, changeListener, rows, props) => {
    const filteredSelected = rows.filter(({ selected }) => selected)[0];
    const fsVal = filteredSelected ? filteredSelected.value : null;
    const pickerValue = props.pickerValue || fsVal;

    if (!pickerValue) return null;
    return (
      <SPicker
        label={label}
        pickerValue={pickerValue}
        rows={rows}
        changeListener={changeListener}
      />
    );
  }
);
export default PickerList;
