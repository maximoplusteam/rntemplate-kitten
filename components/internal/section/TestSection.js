import React from "react";
import TextField from "./TextField";
import { View, ScrollView } from "react-native";

const testData = [
  {
    type: "ALN",
    fieldKey: "PO",
    label: "PO #",
    listener: console.log,
    value: "1234",
    enabled: true,
  },
  {
    type: "ALN",
    fieldKey: "DESCRIPTION",
    label: "Description",
    listener: console.log,
    value: "Test Purchase Order",
    enabled: true,
    showLookupF: () => {
      console.log("bla");
    },
  },
  {
    type: "DATE",
    fieldKey: "CREATEDATE",
    label: "Created On",
    listener: console.log,
    value: new Date(),
    enabled: true,
  },
  {
    type: "ALN",
    fieldKey: "VENDORPHONE",
    label: "Vendor Phone",
    listener: console.log,
    value: "055 1233456",
    enabled: true,
    metadata: { phonenum: true },
  },
];
const TestSection = () => {
  return (
    <ScrollView>
      {testData.map((t) => (
        <TextField {...t} key={t.fieldKey} />
      ))}
    </ScrollView>
  );
};

export default TestSection;
