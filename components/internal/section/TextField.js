import React from "react";
import {
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
} from "react-native";
import { Datepicker, Icon, Layout, Input } from "@ui-kitten/components";
import { Linking } from "react-native";
import { openBarcodeScan } from "../../../dialogs/utils";
import moment from "moment";
import { DATE_FORMAT, DATETIME_FORMAT } from "@env";
//const DATE_FORMAT = "DD/MM/YYYY";
//const DATETIME_FORMAT = "DD/MM/YYYY hh:mm a";

const TextField = (props) => {
  const {
    type,
    fieldKey,
    label,
    changeListener,
    immediateChangeListener,
    enabled,
    required,
    value,
    listener,
    showLookupF,
    metadata,
  } = props;

  const CalendarIcon = (props) => <Icon {...props} name="calendar" />;

  if (type === "DATE" || type === "DATETIME") {
    console.log(DATETIME_FORMAT);
    console.log(DATE_FORMAT);
    console.log("###");
    const val =
      value &&
      (type === "DATE"
        ? moment(value, DATE_FORMAT).toDate()
        : moment(value, DATETIME_FORMAT).toDate());
    return (
      <Datepicker
        label={required ? "* " + label : label}
        key={fieldKey}
        accessoryRight={CalendarIcon}
        date={val}
        onSelect={function (val) {
          console.log(val);
          return immediateChangeListener(val);
        }}
      ></Datepicker>
    );
  }
  const ShowListButton = (props) => {
    if (metadata && metadata.phonenum) {
      const phoneF = () => Linking.openURL(`tel:${value}`);
      return (
        <TouchableWithoutFeedback onPress={phoneF}>
          <Icon {...props} name="phone" />
        </TouchableWithoutFeedback>
      );
    }

    if (metadata && metadata.barcode) {
      const barcodeF = () =>
        openBarcodeScan((val) => {
          if (changeListener) {
            immediateChangeListener(val);
          } else {
            listener(val);
          }
        });
      return (
        <TouchableWithoutFeedback onPress={barcodeF}>
          <Icon {...props} name="upload-outline" />
        </TouchableWithoutFeedback>
      );
    }
    if (!showLookupF) return null;
    return (
      <TouchableWithoutFeedback onPress={showLookupF}>
        <Icon {...props} name="arrow-ios-downward-outline" />
      </TouchableWithoutFeedback>
    );
  };

  return (
    <Input
      key={fieldKey}
      label={required ? "* " + label : label}
      onChangeText={(val) => listener(val)}
      onBlur={(_) => {
        if (changeListener) {
          changeListener();
        }
      }}
      value={value}
      accessoryRight={ShowListButton}
    />
  );
};

export default TextField;
