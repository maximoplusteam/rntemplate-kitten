import {getSimpleList} from "mplus-react";
import MPList from "./internal/list/MPList";

export default getSimpleList(MPList);
