import TextField from "./internal/section/TextField";
import Picker from "./internal/section/Picker";
import React from "react";
import { getSection, getQbeSection } from "mplus-react";
import { ScrollView, View, StyleSheet } from "react-native";
import { ButtonGroup, Button } from "@ui-kitten/components";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    flexDirection: "column",
  },
  mainSection: {
    flex: 4,
  },
  buttons: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
});

export const Section = (props) => {
  const SectionComp = buildSection(SectionWrapper);
  return <SectionComp {...props} />;
};

export const buildSection = (SectionWrapper) => {
  return getSection(TextField, Picker, (fields, props) => {
    return <SectionWrapper {...props}>{fields}</SectionWrapper>;
  });
};

export const buildQbeSection = (QBESectionWrapper) => {
  return getQbeSection(
    TextField,
    (fields, buttons, props) => (
      <QBESectionWrapper {...props} buttons={buttons}>
        {fields}
      </QBESectionWrapper>
    ),
    (buttons) => buttons
  );
};

const SectionWrapper = (props) => {
  return (
    <View style={styles.container}>
      <ScrollView style={styles.mainSection}>{props.children}</ScrollView>
    </View>
  );
};

const QbeSectionWrapper = (props) => {
  return (
    <View style={styles.container}>
      <ScrollView style={styles.mainSection}>{props.children}</ScrollView>
      <View style={styles.buttons}>
        <ButtonGroup appearance="outline">
          {props.buttons.map(({ label, key, action }) => (
            <Button onPress={action} key={key}>
              {label}
            </Button>
          ))}
        </ButtonGroup>
      </View>
    </View>
  );
};

export const QbeSection = (props) => {
  const QbeSectionComp = buildQbeSection(QbeSectionWrapper);
  return <QbeSectionComp {...props} />;
};
