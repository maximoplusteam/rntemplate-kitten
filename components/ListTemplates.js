import React from "react";
import { ListItem, Icon, Text } from "@ui-kitten/components";
import { StyleSheet } from "react-native";
const CheckIcon = (props) => <Icon {...props} name="funnel-outline" />;
const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },
});
export default {
  po: ({ DESCRIPTION, PONUM, STATUS, onPress }) => (
    <ListItem
      title={`${PONUM} ${STATUS}`}
      description={DESCRIPTION}
      onPress={onPress}
    />
  ),
  valuelist: ({ VALUE, DESCRIPTION, onPress }) => (
    <ListItem title={VALUE} description={DESCRIPTION} onPress={onPress} />
  ),
  qbevaluelist: ({ _SELECTED, VALUE, DESCRIPTION, onPress }) => (
    <ListItem
      accessoryLeft={() =>
        _SELECTED === "Y" ? (
          <Icon style={styles.icon} fill="#8F9BB3" name="checkmark-outline" />
        ) : null
      }
      title={VALUE}
      description={DESCRIPTION}
      onPress={onPress}
    />
  ),
  doclinks: ({ DESCRIPTION, DOCTYPE, onPress }) => (
    <ListItem title={DESCRIPTION} description={DOCTYPE} onPress={onPress} />
  ),
};
