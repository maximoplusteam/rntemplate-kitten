import React, { useEffect } from "react";
import { Section } from "./Section";
import { Button, Text } from "@ui-kitten/components";
import { View, Platform } from "react-native";
import { getWorkflowDialog } from "mplus-react";
import { showMessage } from "react-native-flash-message";

const WFMessages = ({ messages }) => {
  useEffect(() => {
    if (messages && messages.length > 0) {
      showMessage(messages.join("\n"));
    }
  }, [messages]);
  return null;
};

const WFWrapper = ({ buttons, wfTitle, section, warnings }) => {
  return (
    <View>
      <Text appearance="hint">{wfTitle}</Text>
      {section}
      <WFMessages messages={warnings} />
      {buttons.map(({ buttonKey, title, action }, i) => (
        <View style={{ margin: 5 }} key={buttonKey}>
          <Button title={title} onPress={action} appearance="ghost">
            {title}
          </Button>
        </View>
      ))}
    </View>
  );
};

export default getWorkflowDialog(
  Section,
  (title, section, actions, warnings) => {
    const buttons = Object.keys(actions).map((buttonKey) => {
      return {
        buttonKey,
        title: actions[buttonKey].label,
        action: actions[buttonKey].actionFunction,
      };
    });
    return (
      <WFWrapper
        title={title}
        section={section}
        buttons={buttons}
        warnings={warnings}
      />
    );
  }
);
